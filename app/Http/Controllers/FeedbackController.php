<?php

namespace App\Http\Controllers;

use App\Mail\FeedbackEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function customerFeedback(Request $request) {
        $msg = $request->msg;
        $email   = $request->email;
        $name    = $request->name;
        Mail::to('khurram@polar-ma.com')->send(new FeedbackEmail($name, $email, $msg));
        return 'success';
    }
}
