<meta charset="utf-8"/>
<!-- Page Loader - Needs to be placed in header for loading at top of all content -->
<script type="text/javascript" src="{{asset('front/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{url(asset('front/css/pace-theme-corner-indicator.min.css'))}}" />
{{--<link href="{{asset('front/css/pace-loading-bar.css')}}" rel="stylesheet">--}}
<link rel="stylesheet" type="text/css" href="{{asset('front/css/animate.shipping.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('front/css/ShippingIcon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('front/css/bootstrap.css')}}">
<!--Owl-->
<link href="{{asset('front/css/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('front/css/owl.theme.css')}}" rel="stylesheet">
<link href="{{asset('front/css/owl.transitions.css')}}" rel="stylesheet">
<!-- Main Style -->
<link rel="stylesheet" id="main-style" type="text/css" href="{{asset('front/css/style.css')}}">
<link rel="stylesheet" id="main-style" type="text/css" href="{{asset('front/css/sweetalert.css')}}">