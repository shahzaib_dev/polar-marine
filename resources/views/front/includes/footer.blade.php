<!-- Section Start - Footer -->
<section class='footer bg-black parallax ' id='footer'><div class="bg-overlay"></div><div class="container">
		<div class="row">
			<!-- Text Widget - Start -->
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-widget inviewport animated delay1" data-effect="fadeInUp">
				<div class="logo">
					<div>
						<i class="icon icon-logo blue"></i>
					</div>
					<span class="name">POLAR</span>
					<span class="small">Marine Agency</span>
				</div>
				<p style="text-align: justify">PMA focus on building and nurturing the relationship with you. Because it is all about reliability – when time and cost are inseparable, our customers can depend on us to make the right decisions to deliver their goods efficiently and in a cost-effective manner.</p>
			</div>
			<!-- Text Widget - End -->
			
			<!-- News Widget - Start -->
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 news-widget inviewport animated delay2" data-effect="fadeInUp">
				<h4>Director's Message</h4>
				<div class="headul left-align"></div>
				
				
				<div class="news-wrap row">
					<div class="pic col-md-3 col-xs-3 col-sm-3 col-lg-3">
						<img alt="news-image" class="img-responsive" src="{{asset('front/img/news-1.jpg')}}">
					</div>
					<div class="info col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<h5 class="title"><a href='#'>ROAD FREIGHT AND LOGISTICS AWARD OF THE YEAR</a></h5>
						<div class="date">21st October 2015</div>
					</div>
				</div>
				
				
				{{--<div class="news-wrap row">
					<div class="pic col-md-3 col-xs-3 col-sm-3 col-lg-3">
						<img alt="news-image" class="img-responsive" src="{{asset('front/img/news-2.jpg')}}">
					</div>
					<div class="info col-md-9 col-xs-9 col-sm-9 col-lg-9">
						<h5 class="title"><a href='#'>AIR FREIGHT AND LOGISTICS AWARD OF THE YEAR</a></h5>
						<div class="date">21st October 2015</div>
					</div>
				</div>--}}
			
			
			</div>
			<!-- News Widget - End -->
			
			<!-- Contact Widget - Start -->
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 contact-widget inviewport animated delay3" data-effect="fadeInUp">
				<h4>Contact Us</h4>
				<div class="headul left-align"></div>
				<p>Ugoki Road,Opposite GEPCO Colony <br>Shahabpura Road, Sialkot-51310 Pakistan
				<p>Email: info@polar-ma.com <br> Phone: +92 310 6320958</p>
			</div>
			<!-- Contact Widget - End -->
		</div>
	</div>
	
	<!-- Copyright Bar - Start -->
	<div class="copyright">
		<div class="col-md-12">
			<div class="container">
				<div class="">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 message inviewport animated delay1" data-effect="fadeInUp">
						<span class="">&copy; 2017 All rights reserved by Polar Marine Agency </span>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 social">
						<a target="_blank" href="https://web.facebook.com/Pmaskt/" class="inviewport animated delay1" data-effect="fadeInUp"><i class="icon icon-facebook text-on-primary"></i></a>
						<a target="_blank" href="#" class="inviewport animated delay2" data-effect="fadeInUp"><i class="icon icon-twitter text-on-primary"></i></a>
						<a target="_blank" href="#" class="inviewport animated delay4" data-effect="fadeInUp"><i class="icon icon-google-plus text-on-primary"></i></a>
						<a target="_blank" href="#" class="inviewport animated delay5" data-effect="fadeInUp"><i class="icon icon-youtube-play text-on-primary"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Copyright Bar - End -->


</section>
<!-- Section End - Footer -->
<!-- Site Settings Panel - Start -->
{{--<div class="settings-panel transition collapsed">
	<i class='toggle icon icon-settings'></i>
	<div class="colors">
		<h5>Pick Theme</h5>
		<div data-color="blue" class="blue active" style="background-color: #03a9f4;"><i class='icon icon-check'></i></div>
		<div data-color="red" class="red " style="background-color:#f44336;"><i class='icon icon-check'></i></div>
		<div data-color="green" class="green " style="background-color:#4caf50;"><i class='icon icon-check'></i></div>
		<div data-color="yellow" class="yellow " style="background-color:#ffc107;"><i class='icon icon-check'></i></div>
		<div data-color="orange" class="orange " style="background-color:#ff6630;"><i class='icon icon-check'></i></div>
		
		<div data-color="teal" class="teal " style="background-color:#009688;"><i class='icon icon-check'></i></div>
		<div data-color="purple" class="purple " style="background-color:#7e57c2;"><i class='icon icon-check'></i></div>
		<div data-color="lightgreen" class="lightgreen " style="background-color:#8bc34a;"><i class='icon icon-check'></i></div>
		<div data-color="darkblue" class="darkblue " style="background-color:#3f51b5;"><i class='icon icon-check'></i></div>
		<div data-color="pink" class="pink " style="background-color:#e91e63;"><i class='icon icon-check'></i></div>
	
	</div>
	<div id="settings-response"></div>
</div>--}}
<!-- Site Settings Panel - End -->