
<style>
	.custom_header {
		position: absolute;
		/*width: 100%;*/
		height: auto;
		z-index: 1001;
		top: 0px;
		padding-top: 10px;
		top: 10px;
		right: 2%;
		text-align: right;
	}
	.bg-overlay
	{
		background-color: rgba(33, 33, 33, 0.50) !important;
	}
	.goog-logo-link {
		display:none !important;
		border: none;
		margin: 0px;
	}

	.goog-te-gadget{
		color: transparent !important;
	}
	.goog-te-combo{
		color:black;
		max-width: 130px;
		height: 25px;
		border-radius: 10px;
	}
</style>
<!-- Section Start - Header -->
<div class="custom_header">
	<div id="google_translate_element" ></div>
</div>
<section class='header' id='header'>
	<!-- Header Top Bar - Start -->
	<div class="topbar-wrap">
		<div class="container">
			<div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 logo-area">
				<div class="logo">
					<div style="padding-top: 3%; ">
						<a href="{{url('/')}}" class="logo-text" style="color:white; font-size:25px">ᑭᗝᒪᗩᖇᗰᗩᖇᎥᑎᗴ</a>
					</div>
					
				</div>
			</div>
			<div class="col-lg-9 col-md-8 col-sm-7 col-xs-5 menu-area">
				<!-- Menu Top Bar - Start -->
				<div class="topbar">
					<div class="menu">
						<div class="primary inviewport animated delay2" data-effect="">
							<div class='cssmenu'>
								
								<!-- Menu - Start -->
								<ul class='menu-ul'>
									<li class='has-sub'>
										<a href='{{route('index')}}'>Home</a>
									</li>
									<li class='has-sub'>
										<a href='#'>Our Services <i class='icon icon-chevron-down'></i></a>
										<ul>
											<li >
												<a href='{{route('ourServices')}}'>Air / Sea Freight</a>
											</li>
											
											<li >
												<a href='{{route('ourServices')}}'>Consolidation</a>
											</li>
											<li >
												<a href='{{route('ourServices')}}'>Custom Brokrage</a>
											</li>
											<li >
												<a href='{{route('ourServices')}}'>Road / Rail Mode </a>
											</li>
											
											<li >
												<a href='{{route('ourServices')}}'>Warehousing</a>
											</li>
											<li >
												<a href='{{route('ourServices')}}'>Supply Chain Management</a>
											</li>
										</ul>
									</li>
									<li class='has-sub'>
										<a href='#'>Service Estimate <i class='icon icon-chevron-down'></i></a>
										<ul>
											<li >
												<a href='{{route('serviceEstimate')}}'>CBM Converter</a>
											</li>
											<li >
												<a href='{{route('serviceEstimate')}}'>Volumetric Weight</a>
											</li>
											<li >
												<a href='{{route('serviceEstimate')}}'>CFT Calculator</a>
											</li>
											<li >
												<a href='{{route('serviceEstimate')}}'>Kilotom Calculator</a>
											</li>
										</ul>
									</li>
									<li class='has-sub'>
										<a href='#'>Tracking <i class='icon icon-chevron-down'></i></a>
										<ul>
											<li >
												<a href='{{route('trackAirlines')}}'>Air Cargo</a>
											</li>
											<li >
												<a href='{{route('trackCargo')}}'>Sea Cargo</a>
											</li>
										</ul>
									</li>
									<li class='has-sub'>
										<a href='{{route('aboutUs')}}'>About Us</a>
									</li>
									<li class='has-sub'>
										<a href='{{route('contactUs')}}'>Contact Us</a>
									</li>
								{{--	<li class="has-sub">
										<div id="google_translate_element" style="padding-top:9%"></div>
									</li>--}}
									
								</ul>
								<!-- Menu - End -->
							</div>
						</div>
					</div>
				</div>
				<!-- Menu Top Bar - End -->
				
				
				<!-- Mobile Menu - Start -->
				<div class="menu-mobile col-xs-10 pull-right cssmenu">
					<i class="icon icon-menu menu-toggle"></i>
					<ul class="menu" id='parallax-mobile-menu'>
					
					</ul>
				</div>
				<!-- Mobile Menu - End -->
			</div>
			
		</div>
	</div>
	<!-- Header Top Bar - End -->
	
	@if(Request::path() == '/')
		<div class="header-bg">
			<div id="header-slider" class="owl-carousel">
				<div style="position:relative;display:inline-block;width:100%;height:auto;">
					<img src="{{asset('front/img/main-slider-4.jpg')}}" alt="Header Image" class="img-responsive">
					<div class="bg-overlay"></div>
				</div>
				<div style="position:relative;display:inline-block;width:100%;height:auto;">
					<img src="{{asset('front/img/main_slider_1.jpg')}}" alt="Header Image" class="img-responsive">
					<div class="bg-overlay"></div>
				</div>
				<div style="position:relative;display:inline-block;width:100%;height:auto;">
					<img src="{{asset('front/img/main-slider3.jpg')}}" alt="Header Image" class="img-responsive">
					<div class="bg-overlay"></div>
				</div>
				<div style="position:relative;display:inline-block;width:100%;height:auto;">
					<img src="{{asset('front/img/main_slider_3.jpg')}}" alt="Header Image" class="img-responsive">
					<div class="bg-overlay"></div>
				</div>
			</div>
			<!-- Header Social Icons - Start -->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 social-wrap hidden-xs">
				<div class="social-box">
					<div class="social-icons-wrap">
						<a target="_blank" href="https://web.facebook.com/Pmaskt/"><i class="icon icon-facebook text-on-primary"></i></a>
						<a target="_blank" href="#"><i class="icon icon-twitter text-on-primary"></i></a>
						<a target="_blank" href="#"><i class="icon icon-google-plus text-on-primary"></i></a>
					</div>
				</div>
			</div>
			<!-- Header Social Icons - End -->
		</div>
		@else
			<div class="header-bg header-small">
			
			<!-- Header Content Slide - Start -->
			<div style="position:relative;display:inline-block;width:100%;height:auto;">
				<img src="{{asset('front/img/main-slider.jpg')}}" alt="Header Image" class="img-responsive">
				<div class="bg-overlay"></div>
				<div class="main-wrap">
					<div class="container">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 main-content">
							@if(Request::url() == route('trackCargo'))
								<h1>Track Cargo</h1>
							@elseif(Request::url() == route('trackAirlines'))
								<h1>Track Airlines</h1>
							@elseif(Request::url() == route('serviceEstimate'))
								<h1>Service Estimate</h1>
							@elseif(Request::url() == route('aboutUs'))
								<h1>About Us</h1>
							@elseif(Request::url() == route('contactUs'))
								<h1>Contact Us</h1>
							@elseif(Request::url() == route('ourServices'))
								<h1>Our Services</h1>
							@endif
							<div class="headul"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- Header Content Slide - End -->
		</div>
	@endif
</section>
<!-- Section End - Header -->