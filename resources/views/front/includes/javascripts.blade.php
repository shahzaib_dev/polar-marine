<!-- Javascript & CSS Files moved to bottom of page for faster loading -->
<script type="text/javascript" src="{{asset('front/js/jquery.min.js')}}"></script>
<script src="{{asset('front/js/inviewport-1.3.2.js')}}"></script>


<!--Owl-->
<script src="{{asset('front/js/owl.carousel.min.js')}}"></script>


<!-- Google Map -->
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js{{--?key=AIzaSyAQS2s8m7Re6rR4-tjlFtmWQBZJMfu5QKY--}}"></script>
<script type="text/javascript" src="{{asset('front/js/gmap.js')}}"></script>


<!--Fancybox -->
<script type="text/javascript" src="{{asset('front/js/jquery.fancybox.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/jquery.fancybox-media.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('front/css/jquery.fancybox.css')}}" media="screen"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.js"></script>


<!-- Main JS -->
{{--<script type="text/javascript" src="{{asset('front/js/main.js')}}"></script>--}}
@include('front.includes.main')
<script type="text/javascript" src="{{asset('front\js\sweetalert.min.js')}}"></script>

<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en',
            layout: google.translate.TranslateElement.FloatPosition.BOTTOM_RIGHT
        }, 'google_translate_element');
    }
</script>
<script type="text/javascript"
        src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        
        