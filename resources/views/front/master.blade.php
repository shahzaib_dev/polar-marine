<!DOCTYPE html>
<html>

<!-- Mirrored from jaybabani.com/shipping-html/about-us-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:38:27 GMT -->
<head>
	<title>@yield('title')</title>
	@include('front.includes.stylesheets')
	@yield('stylesheet')
</head>
<body class="blue page-loading">
@include('front.includes.header')
@yield('content')

@include('front.includes.footer')
@include('front.includes.javascripts')
@yield('javascripts')

</body>

<!-- Mirrored from jaybabani.com/shipping-html/home-slider.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:37:27 GMT -->
</html>