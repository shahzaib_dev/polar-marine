@extends('front.master')
@section('title', 'Home Page')

@section('content')
	<!-- Section Start - Major Services -->
	<section class='major-services gray-boxes' id='major-services'><div class="container">
			<div class="row">
				<h1 class="heading">Our Services</h1>
				<div class="headul"></div>
				<p class="subheading">If you're going to ship something, ship it right. Call <b>Polar Marine Agency</b> and get a fast freight quote to change the way you ship today.</p>
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay1" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/sea_air_freight.png')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Air / Sea Freight" data-fancybox-group="service-gallery" href="{{asset('front/img/sea_air_freight.png')}}"><i class="icon icon-image-area"></i></a>
							</div>
						</div>
						<div class="info">
							<h4 class="title">Air / Sea Freight </h4>
							<p style="text-align: justify">
								<b>Polar Marine Agency</b> Sea-Air service is an effective balance between the speed of air transport and the economical benefits of ocean transport. Documentation and Proof of delivery (POD).
								End-to-end track and trace visibility. We provide you with a diverse range of integrated solutions fully tailored to meet all your needs.
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
				
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay1" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/consolidation.jpg')}}" style="height:308px !important; object-fit:cover">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Consolidation" data-fancybox-group="service-gallery" href="{{asset('front/img/consolidation-card1.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>
						</div>
						<div class="info">
							<h4 class="title">Consolidation</h4>
							<p style="text-align: justify">
								<b>Polar Marine Agency</b> is able to accommodate shipper of any size with its LCL cargo service. <b>PMA</b> consolidates its cargo from smaller shipments into full container loads (FCL). The containers are then shipped in mass at substantial cost savings to our clients due to our volume as the LCL shipments leader.
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
				
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay2" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/custom-brokrage-card.jpg')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Custom Brokerage" data-fancybox-group="service-gallery" href="{{asset('front/img/custom-brokrage-card.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>{{--
							<div class="more">
								<a href="#">Read More</a>
							</div>--}}
						</div>
						<div class="info">
							<h4 class="title">Custom Brokerage</h4>
							<p style="text-align: justify">
								Import / export customs clearance service at all ports / CFS in Pakistan, Customs evaluation(Physical Inspection) of merchandise, Import duty & tax assessment , Handling & Consultancy on Made In Issue / Rejected Cargo, Classification of HS Codes & Online Entries, Temporary / Permanent import/export procedures
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
				
			
			</div>
			
			<div class="row" style="margin-top:5%">
				
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay2" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/service-2.jpg')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Road Transportation Service" data-fancybox-group="service-gallery" href="{{asset('front/img/service-2.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>{{--
							<div class="more">
								<a href="#">Read More</a>
							</div>--}}
						</div>
						<div class="info">
							<h4 class="title">Road / Rail Transportation</h4>
							<p style="text-align: justify">
								<b>Polar Marine Agency</b> has a flexible, reliable and efficient inland / overland transportation offering, including Road, Rail and Intermodal Services. We have a global capability with services operating in Europe, North Africa, Middle East, Asia Pacific and Americas..
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
								
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay3" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/warehousing-card.jpg')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Warehousing & Distribution" data-fancybox-group="service-gallery" href="{{asset('front/img/warehousing-card.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>
						</div>
						<div class="info">
							<h4 class="title">Warehousing & Distribution</h4>
							<p style="text-align: justify">
								<b>POLAR MARINE AGENCY</b> evaluate the benefits of location, size and design, factoring in where your sources of supply and delivery destinations lie.
								We manage and operate a warehouse and an open storage yard
								We offer every combination of warehousing solution that your supply chain might require
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
				
				
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay3" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/Supply-Chain-Management-Keyboard.jpg')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Ocean Freight Service" data-fancybox-group="service-gallery" href="{{asset('front/img/Supply-Chain-Management-Keyboard.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>{{--
							<div class="more">
								<a href="#">Read More</a>
							</div>--}}
						</div>
						<div class="info">
							<h4 class="title">Supply Chain Management</h4>
							<p style="text-align: justify">
								Our services are designed to support your supply chain management and comprise the design, planning and management of activities i.e sourcing, procurement, execution and control of logistics activities within an end-to-end supply chain. The objective is to create sustainable value across the entire supply chain and build a competitive infrastructure.
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
			</div>
			
			
		</div></section>
	<!-- Section End - Major Services -->
	
@endsection

@section('javascripts')
	
@endsection