@extends('front.master')
@section('title', 'Home Page')

@section('content')
	<!-- Section Start - Major Services -->
	<section class='major-services gray-boxes' id='major-services'>
		<div class="container">
			<div class="row">
				<h1 class="heading">Major Services</h1>
				<div class="headul"></div>
				<p class="subheading">If you're going to ship something, ship it right. Call <b>Polar Marine Agency</b> and get a fast freight quote to change the way you ship today.</p>
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay1" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/service-1.jpg')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Air / Sea Freight" data-fancybox-group="service-gallery" href="{{asset('front/img/service-1.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>{{--
							<div class="more">
								<a href="#">Read More</a>
							</div>--}}
						</div>
						<div class="info">
							<h4 class="title">Air / Sea Freight </h4>
							<p style="text-align: justify">
								<b>Polar Marine Agency</b> Sea-Air service is an effective balance between the speed of air transport and the economical benefits of ocean transport. Documentation and Proof of delivery (POD).
								End-to-end track and trace visibility. We provide you with a diverse range of integrated solutions fully tailored to meet all your needs.
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
				
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay1" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/consolidation.jpg')}}" style="height:308px !important; object-fit:cover">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Consolidation" data-fancybox-group="service-gallery" href="{{asset('front/img/consolidation.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>
						</div>
						<div class="info">
							<h4 class="title">Consolidation</h4>
							<p style="text-align: justify">
								<b>Polar Marine Agency</b> is able to accommodate shipper of any size with its LCL cargo service. <b>PMA</b> consolidates its cargo from smaller shipments into full container loads (FCL). The containers are then shipped in mass at substantial cost savings to our clients due to our volume as the LCL shipments leader.
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
				
				<!-- Service - Start -->
				<div class="col-lg-4 col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 service inviewport animated delay2" data-effect="fadeInUp">
					<div class="service-wrap">
						<div class="pic">
							<img alt="service-image" class="img-responsive" src="{{asset('front/img/custom-brokrage-card.jpg')}}">
							<div class="info-layer transition">
								<a class="btn btn-primary fancybox" title="Custom Brokerage" data-fancybox-group="service-gallery" href="{{asset('front/img/custom-brokrage-card.jpg')}}"><i class="icon icon-image-area"></i></a>
							</div>{{--
							<div class="more">
								<a href="#">Read More</a>
							</div>--}}
						</div>
						<div class="info">
							<h4 class="title">Custom Brokerage</h4>
							<p style="text-align: justify">
								Import / export customs clearance service at all ports / CFS in Pakistan, Customs evaluation(Physical Inspection) of merchandise, Import duty & tax assessment , Handling & Consultancy on Made In Issue / Rejected Cargo, Classification of HS Codes & Online Entries, Temporary / Permanent import/export procedures
							</p>
						</div>
					</div>
				</div>
				<!-- Service - End -->
			
			</div>
			<div class="row">
				<div class="col-md-12" style="text-align: center;padding-top:5%">
					<a href="{{route('ourServices')}}" class="btn btn-info" >View All</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Section End - Major Services -->
	
	<!-- Section Start - Worldwide Centres -->
	<section class='bg-black' id='worldwide'><div class="container">
			<div class="row">
				<h1 class="heading text-white">Worldwide Centres</h1>
				<div class="headul"></div>
				{{--<p class="subheading text-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit enean commodo  eget dolor aenean massa eget dolor aenean massa</p>--}}
				<div class="worldwide col-md-12 col-xs-12">
					<div class="map">
						<img src="{{asset('front/img/map.png')}}" class="img-responsive" alt="Map image">
					</div>
					<div class="map-locations inviewport animated delay2" data-effect="fadeIn">
						<img src="{{asset('front/img/map-locations-blue.png')}}" class="img-responsive style-dependent" alt="Map Locations">
					</div>
					<div class="map-connect inviewport animated delay6" data-effect="fadeIn">
						<img src="{{asset('front/img/map-connect-blue.png')}}" class="img-responsive style-dependent" alt="Map Connections">
					</div>
				
				</div>
			</div>
		</div></section>
	<!-- Section End - Worldwide Centres -->
	
	
	<!-- Section Start - Service Estimate -->
	<section class='estimate' id='estimate'>
		<div class="estimate-wrap">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-10  col-xs-offset-1 col-xs-10 ">
					<h1 class="heading left-align">Service Estimate</h1>
					<div class="headul left-align"></div>
					<p class="subheading left-align">
						
					</p>
				</div>
				
				<div class="col-lg-4 col-md-4 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-10  col-xs-offset-1 col-xs-10 ">
					
					<!-- Estimate Form - Start -->
					<div class="row">
						<form>
							<div class='col-xs-12'>
								<label>Calculate KiloTon (KT)</label>
							</div>
							<div class='col-xs-12'>
								<label>Pound(s)</label>
								<input type='number' name="pound_value" placeholder='' class='transition' id='pound_value'>
							</div>
							<div class='col-xs-12'>
								<label>Total kiloton</label>
								<input type='text' id="result_of_kiloton" placeholder='Result Will Be Displayed Here' class='transition' readonly>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<label>Calculate in cubic meter, cubic feet, volumetric weight</label>
						</div>
					</div>
					<div class='row'>
						<form action="{{url('/')}}" method="get" id="calculationForm">
							<div class='col-xs-4'>
								<label>Length</label>
								<input type='text' placeholder='' class='transition' id='est_length' required>
							</div>
							<div class='col-xs-4'>
								<label>Width</label>
								<input type='text' placeholder='' class='transition' id='est_width' required>
							</div>
							<div class='col-xs-4'>
								<label>Height</label>
								<input type='text' placeholder='' class='transition' id='est_height' required>
							</div>
						</form>
					</div>
					<div class="row">
						<form>
							<div class='col-xs-12'>
								<label for="calculate">Calculate</label>
								<select name="calculate" id="calculate" class="transition" style="width:100%" required>
									<option value="">Please Select</option>
									<option value="cubic_meter">Cubic Meter (m3)</option>
									<option value="cubic_feet">Cubic feet (CFT)</option>
									<option value="volumetric_weight">Volumetric Weight (In Kilograms)</option>
								</select>
							</div>
							<div class='col-xs-12' >
								<span id="calculate_cbm_span" style="display:none">
									<label for="calculate_cbm">Calculate In</label>
									<select name="calculate_cbm" id="calculate_cbm" class="transition" style="width:100%" required>
										<option value="">Please Select</option>
										<option value="cbm_centimeters">Centimeters</option>
										<option value="cbm_meters">Meters</option>
									</select>
								</span>
								
								<span id="calculate_volumetric_span" style="display:none">
									<label for="calculate_volumetric">Calculate In</label>
									<select name="calculate_volumetric" id="calculate_volumetric" class="transition" style="width:100%" required>
										<option value="">Please Select</option>
										<option value="volumetric_centimeters">Centimeters</option>
										<option value="volumetric_inches">Inches</option>
									</select>
								</span>
							
							</div>
							<div class='col-xs-12'>
								<label id="result_label">Calculation Result</label>
								<input type='text' placeholder='' class='transition' id='calculation_result' readonly>
							</div>
						</form>
					</div>
					<!-- Estimate Form - End -->
				</div>
				
				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 pic">
					<img src="{{asset('front/img/estimate-fork-blue.jpg')}}" class="img-responsive style-dependent" alt="Estimate Fork Image">
				</div>
			</div>
		</div>
	
	</section>
	<!-- Section End - Service Estimate -->
	

@endsection

@section('javascripts')
	<script>
		var length, width, height;
		$('#calculate').change(function(e){
            length   = $('#est_length').val();
            width    = $('#est_width').val();
            height   = $('#est_height').val();
		    if($(this).val().length > 0)
		    {
                var calculate = $(this).val();
                var formValidated = $('#calculationForm').parsley().validate();
                console.log(formValidated);
                if(formValidated)
                {
                    if(calculate == 'cubic_meter')
                    {
                        $('#calculate_volumetric_span').hide();
                        $('#calculate_cbm_span option[value=""]').prop('selected', true);
	                    $('#calculation_result').val('');

                        $('#calculate_cbm_span').show();
                    }
                    else if(calculate == 'volumetric_weight')
                    {
                        $('#calculate_cbm_span').hide();
                        $('#calculate_volumetric_span option[value=""]').prop('selected', true);
	                    $('#calculation_result').val('');

                        $('#calculate_volumetric_span').show();
                    }
                    else if(calculate == 'cubic_feet')
                    {
                        $('#calculate_cbm_span').hide();
                        $('#calculate_volumetric_span').hide();
                        var cubic_feet_result = (length * width * height) / 1728;
                        $('#calculation_result').val(cubic_feet_result);
                    }
                    else
                    {
                        $('#calculate_cbm_span').hide();
                        $('#calculate_volumetric_span').hide();
                        $('#calculation_result').val('');

                    }
                }
                else
                {
                    $('#calculation_result').val('');
                }
		    }
		    else
		    {
                $('#calculate_cbm_span').hide();
                $('#calculate_volumetric_span').hide();
                $('#calculation_result').val('');
		    }
		});
		$('#calculate_cbm').change(function(){
            length   = $('#est_length').val();
            width    = $('#est_width').val();
            height   = $('#est_height').val();
		    
		   var calculate_cbm_in  = $(this).val();
            
		   if(calculate_cbm_in == 'cbm_centimeters')
		   {
		       var cbm_result_in_centimeters = (length * width * height) / 6000;
		       $('#calculation_result').val(cbm_result_in_centimeters);
		   }
		   else if(calculate_cbm_in == 'cbm_meters')
		   {
		       var cbm_result_in_meters = length * width * height / 366;
		       $('#calculation_result').val(cbm_result_in_meters);

           }
		   else
		   {
               $('#calculation_result').val('');
		   }
		});
		
		$('#calculate_volumetric').change(function(){
            length   = $('#est_length').val();
            width    = $('#est_width').val();
            height   = $('#est_height').val();
            
		   var calculate_cbm_in  = $(this).val();
		   if(calculate_cbm_in == 'volumetric_centimeters')
		   {
		       var volume_result_in_centimeters = (length * width * height) / 1000000;
		       $('#calculation_result').val(volume_result_in_centimeters);

           }
		   else if(calculate_cbm_in == 'volumetric_inches')
		   {
		       var cbm_result_in_meters = length * width * height;
		       $('#calculation_result').val(cbm_result_in_meters);
           }
		   else
		   {
               $('#calculation_result').val('');
		   }
		});

        $('#est_length, #est_width, #est_height').keyup(function(){
            if($('#calculate').val() == 'cubic_feet')
            {
                length   = $('#est_length').val();
                width    = $('#est_width').val();
                height   = $('#est_height').val();
                $('#calculate_cbm option[value=""]').prop('selected', true);
                $('#calculate_volumetric option[value=""]').prop('selected', true);
                var cubic_feet_result = (length * width * height) / 1728;
                $('#calculation_result').val(cubic_feet_result);
                return false;
            }
            $('#calculate_cbm option[value=""]').prop('selected', true);
            $('#calculate_volumetric option[value=""]').prop('selected', true);
            $('#calculation_result').val('');
        });
        
        $('#pound_value').keyup(function(){
            var pounds = $(this).val();
            var total_kiloton = pounds /2204.62;
            $('#result_of_kiloton').val(total_kiloton.toFixed(3));
        });
       
	</script>
@endsection