@extends('front.master')
@section('title', 'Home Page')

@section('content')
    <!-- Section Start - Get In Touch -->
    <section class='contact' id='contact'>
        <div class="container">
            <div class="row">
                <h1 class="heading">Get In Touch</h1>
                <div class="headul"></div>
                <p class="subheading">For your import and export by sea/air quries, Contact on below mention detail</p>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">

                    <!-- Contact Form - Start -->
                    <div class='row'>
                        <form action="{{url('get/feedback')}}" method='post'>
                            <div class='col-xs-12'>
                                <input type='text' placeholder='Name' class='transition' id='c_name'>
                            </div>
                            <div class='col-xs-12'><input type='text' placeholder='Email' class='transition'
                                                          id='c_email'></div>
                            <div class='col-xs-12'><textarea class='transition' placeholder='Message'
                                                             id='c_message'></textarea>
                            </div>
                            <div id='response_email' class='col-xs-12'></div>
                            <div class='col-xs-4'>
                                <button type='button' class='btn btn-primary disabled transition' id='c_send'>Send
                                    Message
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- Contact Form - End -->
                </div>


                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-0 contact-full-info">
                    <h5>Main Office</h5>
                    <p>
                        Ugoki Road, Opp. GEPCO Colony<br>
                        Shahabpura Road, Sialkot-51310 Pakistan<br>
                        Email: info@polar-ma.com<br>
                        Phone: +92 333 8731 330
                    </p>
                    <br>
                    <h5>Sub Office</h5>
                    <p>
                        Room No. 41, Mian chamber,<br>
                        Shahar-e-Liaquat, Karachi<br>
                        Email: sktcs@polar-ma.com<br>
                    </p>
                </div>


            </div>
        </div>
    </section>
    <!-- Section End - Get In Touch -->
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div id="contact-map" class="gmap"></div>
        </div>
    </div>

@endsection

@section('javascripts')

@endsection