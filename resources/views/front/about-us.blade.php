@extends('front.master')
@section('title', 'Home Page')

@section('content')
	
	<!-- Section Start - Our Mission -->
	<section class='mission bg-lightgray' id='mission'><div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-area">
					<h1 class="heading left-align">Our Mission</h1>
					<div class="headul left-align"></div>
					<p style="text-align: justify">
						The legacy continues with its philosophy to transport goods quickly, efficiently and economically worldwide. Our main objective is for your products to arrive always on time and in good order, we take away the logistics problems away from you, allowing you to concentrate in your business.
					</p>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 slider-area">
					
					
					<!-- Image Carousel - Start -->
					<ul id="image-slider" class="owl-carousel">
						<!-- Image Carousel Item - Start -->
						<li class="">
							<img alt="mission-carousel-image" class="lazyOwl img-responsive" data-src="{{asset('front/img/our_mission.jpg')}}" src="#">
						</li>
						<!-- Image Carousel Item - End -->
					</ul>
					<!-- Image Carousel - End -->
				
				
				</div>
			</div>
		
		</div></section>
	<!-- Section End - Our Mission -->
	
	<!-- Section Start - Who We Are -->
	<section class='whoweare padding-bottom-50' id='whoweare'><div class="container">	<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 whoweare-pic">
					<img src="{{asset('front/img/our_vision.jpg')}}" class="img-responsive" alt="Who We Are">
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 whoweare-info">
					<h1 class="heading left-align">Our Vision</h1>
					<div class="headul left-align"></div>
					<p style="text-align: justify">
						The core of our vision and our strategy is "cross-selling" — the process of offering customers the solutions they need, when they need them, to help them succeed financially. The more we give our customers what they need, the more we know about them. The more we know about their other logistics needs, the easier it is for them to bring us more of their business. The more business they do with us, the better value they receive, the more loyal they are. The longer they stay with us, the more opportunities we have to satisfy even more of their complete Supply Chain Mgt needs. As a Customised Logistics Solution Provider, we have a clear cost advantage — and a value advantage for our customers — over companies that offer just a few freight forwarding services.
						
						At <b>PMA</b> service are inseparable. More sales do not always lead to better service, but better service almost always leads to more sales. We're in the service business. We inspect what we expect for sales and service performance. We expect all <b>PMA</b> businesses to collaborate, to be partners — referring all their customers to all our other businesses. We expect to sell at least one more product to every customer every year.
					</p>
				</div>
			</div>
		</div></section>
	<!-- Section End - Who We Are -->
@endsection

@section('javascripts')
	
@endsection