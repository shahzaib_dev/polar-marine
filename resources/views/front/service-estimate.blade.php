@extends('front.master')
@section('title', 'Home Page')

@section('content')
	
	<!-- Section Start - Service Estimate -->
	<section class='estimate' id='estimate'>
		<div class="estimate-wrap">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-10  col-xs-offset-1 col-xs-10 ">
					<h1 class="heading left-align">Service Estimate</h1>
					<div class="headul left-align"></div>
					<p class="left-align"><span style="font-size: 50px">How to calculate your cargo</span></p>
					<p>
						<span>Calculate Your Cargo In Cubic Meter, Cubic Feet,Kiloton And Volumetric weight. </span>
					</p>
					<p><b>Load Ability Of Ocean Containers</b></p>
					<ul>
						<li>20'ft container 26-28 CBM</li>
						<li>40'ft container 56-58 CBM</li>
						<li>40'ft HQ container 60-68 CBM</li>
						<li>45'ft HQ container about 78 CBM</li>
					</ul>
					
				</div>
				
				<div class="col-lg-4 col-md-4 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-10  col-xs-offset-1 col-xs-10 ">
					
					<!-- Estimate Form - Start -->
					<div class="row">
						<div class="col-xs-12">
							<span style="font-size: 15px;font-weight: 900;">Calculate in Cubic Meter, Cubic Feet, Volumetric Weight</span>
						</div>
					</div>
					<br>
					<div class='row'>
						<form action="{{url('/')}}" method="get" id="calculationForm">
							<div class='col-xs-4'>
								<label>Length</label>
								<input type='text' placeholder='' class='transition' id='est_length' required>
							</div>
							<div class='col-xs-4'>
								<label>Width</label>
								<input type='text' placeholder='' class='transition' id='est_width' required>
							</div>
							<div class='col-xs-4'>
								<label>Height</label>
								<input type='text' placeholder='' class='transition' id='est_height' required>
							</div>
						</form>
					</div>
					<div class="row">
						<form>
							<div class='col-xs-12'>
								<label for="calculate">Calculate</label>
								<select name="calculate" id="calculate" class="transition" style="width:100%" required>
									<option value="">Please Select</option>
									<option value="cubic_meter">Cubic Meter (m3)</option>
									<option value="cubic_feet">Cubic feet (CFT)</option>
									<option value="volumetric_weight">Volumetric Weight (In Kilograms)</option>
								</select>
							</div>
							<div class='col-xs-12' >
								<span id="calculate_cbm_span" style="display:none">
									<label for="calculate_cbm">Above Values Are In</label>
									<select name="calculate_cbm" id="calculate_cbm" class="transition" style="width:100%" required>
										<option value="">Please Select</option>
										<option value="cbm_centimeters">Centimeters</option>
										<option value="cbm_meters">Meters</option>
									</select>
								</span>
								
								<span id="calculate_volumetric_span" style="display:none">
									<label for="calculate_volumetric">Above Values Are In</label>
									<select name="calculate_volumetric" id="calculate_volumetric" class="transition" style="width:100%" required>
										<option value="">Please Select</option>
										<option value="volumetric_centimeters">Centimeters</option>
										<option value="volumetric_inches">Inches</option>
									</select>
								</span>
							
							</div>
							<div class='col-xs-12'>
								<label id="result_label">Calculation Result</label>
								<input type='text' placeholder='' class='transition' id='calculation_result' readonly>
							</div>
						</form>
					</div>
					<div class="row">
						<form>
							<div class='col-xs-12'>
								<label>Calculate KiloTon (KT)</label>
							</div>
							<div class='col-xs-12'>
								<label>Pound(s)</label>
								<input type='number' name="pound_value" placeholder='' class='transition' id='pound_value'>
							</div>
							<div class='col-xs-12'>
								<label>Total kiloton</label>
								<input type='text' id="result_of_kiloton" placeholder='Result Will Be Displayed Here' class='transition' readonly>
							</div>
						</form>
					</div>
					<!-- Estimate Form - End -->
				</div>
				
				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 pic">
					<img src="{{asset('front/img/estimate-fork-blue.jpg')}}" class="img-responsive style-dependent" alt="Estimate Fork Image">
				</div>
			</div>
		</div>
	
	</section>
	<!-- Section End - Service Estimate -->
	
	
@endsection

@section('javascripts')
	<script>
        var length, width, height;
        $('#calculate').change(function(e){
            length   = $('#est_length').val();
            width    = $('#est_width').val();
            height   = $('#est_height').val();
            if($(this).val().length > 0)
            {
                var calculate = $(this).val();
                var formValidated = $('#calculationForm').parsley().validate();
                console.log(formValidated);
                if(formValidated)
                {
                    if(calculate == 'cubic_meter')
                    {
                        $('#calculate_volumetric_span').hide();
                        $('#calculate_cbm_span option[value=""]').prop('selected', true);
                        $('#calculation_result').val('');

                        $('#calculate_cbm_span').show();
                    }
                    else if(calculate == 'volumetric_weight')
                    {
                        $('#calculate_cbm_span').hide();
                        $('#calculate_volumetric_span option[value=""]').prop('selected', true);
                        $('#calculation_result').val('');

                        $('#calculate_volumetric_span').show();
                    }
                    else if(calculate == 'cubic_feet')
                    {
                        $('#calculate_cbm_span').hide();
                        $('#calculate_volumetric_span').hide();
                        var cubic_feet_result = (length * width * height) / 1728;
                        $('#calculation_result').val(cubic_feet_result);
                    }
                    else
                    {
                        $('#calculate_cbm_span').hide();
                        $('#calculate_volumetric_span').hide();
                        $('#calculation_result').val('');

                    }
                }
                else
                {
                    $('#calculation_result').val('');
                }
            }
            else
            {
                $('#calculate_cbm_span').hide();
                $('#calculate_volumetric_span').hide();
                $('#calculation_result').val('');
            }
        });
        $('#calculate_cbm').change(function(){
            length   = $('#est_length').val();
            width    = $('#est_width').val();
            height   = $('#est_height').val();

            var calculate_cbm_in  = $(this).val();

            if(calculate_cbm_in == 'cbm_centimeters')
            {
                var cbm_result_in_centimeters = (length * width * height) / 1000000;
                $('#calculation_result').val(cbm_result_in_centimeters);
            }
            else if(calculate_cbm_in == 'cbm_meters')
            {
                var cbm_result_in_meters = length * width * height ;
                $('#calculation_result').val(cbm_result_in_meters);

            }
            else
            {
                $('#calculation_result').val('');
            }
        });

        $('#calculate_volumetric').change(function(){
            length   = $('#est_length').val();
            width    = $('#est_width').val();
            height   = $('#est_height').val();

            var calculate_cbm_in  = $(this).val();
            if(calculate_cbm_in == 'volumetric_centimeters')
            {
                var volume_result_in_centimeters = (length * width * height) / 6000;
                $('#calculation_result').val(volume_result_in_centimeters);

            }
            else if(calculate_cbm_in == 'volumetric_inches')
            {
                var cbm_result_in_meters = (length * width * height) / 366;
                $('#calculation_result').val(cbm_result_in_meters);
            }
            else
            {
                $('#calculation_result').val('');
            }
        });

        $('#est_length, #est_width, #est_height').keyup(function(){
            if($('#calculate').val() == 'cubic_feet')
            {
                length   = $('#est_length').val();
                width    = $('#est_width').val();
                height   = $('#est_height').val();
                $('#calculate_cbm option[value=""]').prop('selected', true);
                $('#calculate_volumetric option[value=""]').prop('selected', true);
                var cubic_feet_result = (length * width * height) / 1728;
                $('#calculation_result').val(cubic_feet_result);
                return false;
            }
            $('#calculate_cbm option[value=""]').prop('selected', true);
            $('#calculate_volumetric option[value=""]').prop('selected', true);
            $('#calculation_result').val('');
        });

        $('#pound_value').keyup(function(){
            var pounds = $(this).val();
            var total_kiloton = pounds /2204.62;
            $('#result_of_kiloton').val(total_kiloton.toFixed(3));
        });
	
	</script>
@endsection