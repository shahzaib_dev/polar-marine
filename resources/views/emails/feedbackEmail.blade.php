<div style="color:#313a45;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI','Roboto','Oxygen','Ubuntu','Cantarell','Fira Sans','Droid Sans','Helvetica Neue',sans-serif;font-size:16px;line-height:1.4;margin:0;min-width:100%;padding:0;width:100%!important"
     bgcolor="#f4f4f4">
    <center style="min-width:580px;width:100%">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" class="m_-389800524413294011body"
               style="border-collapse:collapse;border-spacing:0;color:#313a45;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI','Roboto','Oxygen','Ubuntu','Cantarell','Fira Sans','Droid Sans','Helvetica Neue',sans-serif;font-size:16px;line-height:1.4;margin:0;min-width:100%;padding:0;table-layout:fixed;width:100%!important"
               bgcolor="#f4f4f4">
            <tbody>
            <tr style="padding:0">
                <td align="center" valign="top" class="m_-389800524413294011email-container"
                    style="border-collapse:collapse!important;margin:0;min-width:100%;padding:20px 10px;width:100%!important;word-break:break-word">

                    <table border="0" cellpadding="0" cellspacing="0" width="580"
                           class="m_-389800524413294011email-body"
                           style="border-collapse:collapse;border-radius:8px;border-spacing:0;padding:0;table-layout:auto"
                           bgcolor="#fff">


                        <tbody>
                        <tr style="padding:0">
                            <td align="center" valign="middle" class="m_-389800524413294011header"
                                style="border-collapse:collapse!important;border-radius:8px 8px 0 0;
                                height:55px;padding:0;word-break:break-word;color: white"
                                bgcolor="#313945">
                                Polar Marine Agency

                            </td>
                        </tr>


                        <tr style="padding:0">
                            <td align="left" valign="top" class="m_-389800524413294011content"
                                style="border-collapse:collapse!important;padding:50px 50px 40px;word-break:break-word">

                                <h1 class="m_-389800524413294011by-center"
                                    style="font-size:28px;font-weight:bold;line-height:30px;margin:0;padding-bottom:40px;word-break:normal"
                                    align="center">Hi Khurram</h1>

                                <p class="m_-389800524413294011lead m_-389800524413294011by-center"
                                   style="font-size:23px;font-weight:normal;line-height:30px;margin:0;padding-bottom:40px"
                                   align="center">You have new message from contact us page
                                </p>

                                <p>
                                    <span style="font-size:18px;margin:0;padding-bottom:10px"><b>Name: </b></span><span>{{$name}}</span>
                                </p>

                                <p>
                                    <span style="font-size:18px;margin:0;padding-bottom:10px"><b>Email: </b></span><span>{{$email}}</span>
                                </p>

                                <p>
                                    <span style="font-size:18px;margin:0;padding-bottom:10px"><b>Message</b></span>

                                </p>
                                <p style="text-align: justify">
                                    {{$msg}}
                                </p>
                            </td>
                        </tr>


                        <tr style="padding:0">
                            <td align="center" valign="middle" class="m_-389800524413294011footer"
                                style="border-collapse:collapse!important;padding:0 15px 35px;word-break:break-word">


                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                       style="border-collapse:collapse;border-spacing:0;padding:0;table-layout:auto">
                                    <tbody>
                                    <tr style="padding:0">
                                        <td align="center" valign="middle" class="m_-389800524413294011close-text"
                                            style="border-collapse:collapse!important;border-top-color:#ccc;border-top-style:solid;border-top-width:1px;font-size:14px;padding:30px 0 0;word-break:break-word">
                                            <strong>http://www.polar-ma.com/</strong>
                                            <br>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>

                        </tbody>
                    </table>

                </td>
            </tr>
            </tbody>
        </table>
    </center>
    <div class="yj6qo"></div>
    <div class="adL">


    </div>
</div>