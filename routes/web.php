<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as' => 'index', function () {
    return view('front.index');
}]);
Route::get('our/services',['as' => 'ourServices', function () {
    return view('front.our-services');
}]);

Route::get('contact/us',['as' => 'contactUs', function () {
    return view('front.contact-us');
}]);

Route::post('get/feedback',[
    'as' => 'getFeedback',
    'uses' => 'FeedbackController@customerFeedback'
]);

Route::get('about/us',['as' => 'aboutUs', function () {
    return view('front.about-us');
}]);

Route::get('service/estimate',['as' => 'serviceEstimate', function () {
    return view('front.service-estimate');
}]);

Route::get('track/airlines',['as' => 'trackAirlines', function () {
    return view('front.track-airlines');
}]);

Route::get('track/cargo',['as' => 'trackCargo', function () {
    return view('front.track-cargo');
}]);
